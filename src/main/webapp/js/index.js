$(function () {
    let n = 0;
    let b = $(".self-banner-img li").length;
    let c = b * 100;
    let d = 1 / b * 100;
    $(".self-banner-img").width(c+"%");
    $(".self-banner-img li").width(d+"%");
    $(".self-banner-list").width(b*30);

    /* Add Points */
    if(b > 1) {
        for(let i=0; i<b; i++) {
            let listSpan = $("<span></span>")
            $(".self-banner-list").append(listSpan);
        }
    }
    $(".self-banner-list span:eq(0)").addClass("spcss").siblings("span").removeClass("spcss");

    /* Auto Scroll */
    function rollEvent() {
        if(n == b - 1) {
            let ctPosit = (n + 1) * 100;
            $(".self-banner").append($(".self-banner-img").clone());
            $(".self-banner-img:last").css("left", "100%");
            $(".self-banner-img:first").animate({
                "left": "-" + ctPosit + "%"
            }, 1000);
            $(".self-banner-img:last").animate({
                "left": "0"
            }, 1000);
            let setTime0 = setTimeout(function() {
                $(".self-banner .self-banner-img:first").remove();
            }, 1000);
            n = 0;
            $(".self-banner-list span:eq(0)").addClass("spcss").siblings("span").removeClass("spcss");
        } else {
            n++;
            let ctPosit = n * 100;
            $(".self-banner-img").animate({
                "left": "-" + ctPosit + "%"
            }, 1000);
            $(".self-banner-list span:eq(" + n + ")").addClass("spcss").siblings("span").removeClass("spcss");
        }
    }

    /* Set Banner Roll Time */
    let slidesetInterval = setInterval(rollEvent, 3000);

    /* On Hover Banner Stops Scroll*/
    $(".self-banner").hover(function() {
        clearInterval(slidesetInterval);
    }, function() {
        slidesetInterval = setInterval(rollEvent, 3000);
    })

    $(".banner-arrow-right").click(function() {
        if(n === b - 1) {
            let ctPosit = (n + 1) * 100;
            $(".self-banner").append($(".self-banner-img").clone());
            $(".self-banner-img:last").css("left", "100%");
            $(".self-banner-img:first").animate({
                "left": "-" + ctPosit + "%"
            }, 1000);
            $(".self-banner-img:last").animate({
                "left": "0"
            }, 1000);
            let setTime0 = setTimeout(function() {
                $(".self-banner .self-banner-img:first").remove();
            }, 1000);
            n = 0;
            $(".self-banner-list span:eq(0)").addClass("spcss").siblings("span").removeClass("spcss");
        } else {
            n++;
            let ctPosit = n * 100;
            $(".self-banner-img").animate({
                "left": "-" + ctPosit + "%"
            }, 1000);
            $(".self-banner-list span:eq(" + n + ")").addClass("spcss").siblings("span").removeClass("spcss");
        }
    });

    /*banner_left按钮*/
    $(".banner-arrow-left").click(function() {
        if(n == 0) {
            let stPosit = b * 100;
            let etPosit = (b - 1) * 100;
            $(".self-banner").prepend($(".self-banner-img").clone());
            $(".self-banner-img:first").css("left", "-" + stPosit + "%");
            $(".self-banner-img:last").animate({
                "left": "100%"
            }, 1000);
            $(".self-banner-img:first").animate({
                "left": "-" + etPosit + "%"
            }, 1000);
            let setTime0 = setTimeout(function() {
                $(".self-banner .self-banner-img:last").remove();
            }, 1000);
            n = b - 1;
            $(".self-banner-list span:eq(" + n + ")").addClass("spcss").siblings("span").removeClass("spcss");
        } else {
            n--;
            let ctPosit = n * 100;
            $(".self-banner-img").animate({
                "left": "-" + ctPosit + "%"
            }, 1000);
            $(".self-banner-list span:eq(" + n + ")").addClass("spcss").siblings("span").removeClass("spcss");
        }
    });
    /*焦点按钮*/
    $(".self-banner-list span").click(function() {
        let indexS = $(this).index();
        n = indexS;
        let ctPosit = n * 100;
        $(".self-banner-img").animate({
            "left": "-" + ctPosit + "%"
        }, 1000);
        $(this).addClass("spcss").siblings("span").removeClass("spcss");
    })
});

/*left right 按钮动画*/
$(".self-banner").mouseover(function() {
    $(".banner-arrow-left").css({
        "left": "25px"
    })
    $(".banner-arrow-right").css({
        "right": "25px"
    })
});
$(".self-banner").mouseleave(function() {
    $(".banner-arrow-left").css({
        "left": "-60px"
    })
    $(".banner-arrow-right").css({
        "right": "-60px"
    })
});

/*将img标签转换成背景图显示*/
/*$(function() {
    $(".self-banner-img img").css("display", "none")
    $(".self-banner-img li").each(function(e) {
        $(".self-banner-img li:eq(" + e + ")").css("backgroundImage", "url(" + $(".self-banner-img li:eq(" + e + ")").find("img").attr("src") + ")");
    });
});*/

$(function () {
    $(".header-nav>.header-nav-item:has(ul)").hover(
        function(){
            $(this).find('ul').slideDown(500);
        },
        function(){
            $(this).find('ul').slideUp(500);
        }
    )
});


    // $(function () {
    //     $(window).scroll(function () {
    //         let top1 = $(this).scrollTop();
    //         if(top1 > 1) {
    //             $(".btn_top").stop().fadeIn(200);
    //         } else {
    //             $(".btn_top").stop().fadeOut(200);
    //         }
    //     });
    //     $(".btn-top").click(function () {
    //          $("body,html").animate({scrollTop: 0}, 500);
    //         // $('#to_top').animate({height: "300px"}, 500);
    //         // console.log(document.body.scrollTop)
    //         // document.body.scrollTop = 0
    //         // document.documentElement.scrollTop = 0
    //         // console.log(document.body.scrollTop)
    //     });
    // });


