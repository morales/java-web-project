<%--
  Created by IntelliJ IDEA.
  User: Havoc_Wei
  Date: 2021/12/16
  Time: 15:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
    <c:url var="base" value="/"/>
    <base href="${base}">
    <meta charset="UTF-8">
    <title>添加就业信息</title>
    <link rel="icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="shortcut icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="bookmark" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link href="https://cdn.bootcss.com/material-design-icons/3.0.1/iconfont/material-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://getbootstrap.net/Application/Home/View/Public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=basePath%>layui/css/layui.css" type="text/css">
    <script src="<%=basePath%>js/jquery-3.5.1.min.js" charset="UTF-8"></script>
    <script src="https://cdn.staticfile.org/popper.js/2.9.3/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.min.js"></script>
    <script src="<%=basePath%>layui/layui.js" charset="UTF-8"></script>
    <style>
        * {
            /*padding: 0 !important;*/
            /*margin: 0 !important;*/
            box-sizing: border-box !important;
        }

        a {
            text-decoration: none  !important;
        }

        .self-container {
            height: 100vh ;
            background-color: white ;
        }

        .self-row {
            display: flex !important;
            align-items: flex-start ;
            background-color: #F0F0F0 ;
        }

        .self-row .grow-self {
            flex-grow: 1 ;
            height: auto ;
            padding: 20px ;
        }

        .self-nav ul {
            display: flex !important;
            background: #333333 !important;
            list-style: none !important;
            margin: 0 !important;
        }

        .self-nav ul a {
            color: white !important;
            text-decoration: none !important;
            display: block !important;
            padding: 15px 25px !important;
        }

        .self-nav ul a:hover {
            background: #111 !important;
        }

        .self-nav .self-right {
            margin-left: auto  !important;
            display: flex ;
            flex-direction: row ;
        }

        .footer {
            height: 80px ;
            bottom: 0 ;
            width: 100% ;
            margin: auto ;
            text-align: center ;
            background-color: #333333 ;
            color: white ;
        }

        .footer p{
            padding: 0 0 !important;
            margin: 0 !important;
        }
        .col-md-1 {width: 8.33%; }
        .col-md-2 {width: 16.67%; }
        .col-md-3 {width: 25%; }
        .col-md-4 {width: 33.33%; }
        .col-md-5 {width: 41.67%; }
        .col-md-6 {width: 50%; }
        .col-md-7 {width: 58.33%; }
        .col-md-8 {width: 66.67%; }
        .col-md-9 {width: 75%; }
        .col-md-10 {width: 83.33%; }
        .col-md-11 {width: 91.67%; }
        .col-md-12 {width: 100%; padding: 0; margin: 0}

        .modal-self-table {
            width: 100% !important;
            border-collapse: collapse !important;
        }

        .btn-change {
            color: #fff ;
            background-color: #007bff ;
            border-color: #007bff ;
            padding: 10px 25px  !important;
            text-decoration: none ;
            display: inline-block ;
            border-radius: 8px ;
        }

        .btn-change:hover {
            color: #fff ;
            background-color: #0069d9 ;
            border-color: #0062cc ;
        }

        .btn-delete {
            color: #fff !important;
            background-color: #dc3545 ;
            border-color: #dc3545 ;
            padding: 10px 25px  !important;
            text-decoration: none ;
            display: inline-block ;
            border-radius: 8px ;
        }

        .btn-delete:hover {
            color: #fff ;
            background-color: #c82333 ;
            border-color: #bd2130 ;
        }

        .modal-form {
            display: flex !important;
            flex-direction: column !important;
        }

        .self-news {
            margin: 20px 20px;
            padding: 20px 20px;
        }
    </style>
</head>
<body>
<div class="self-container">
    <div class="self-area self-row">
        <div class="col-md-12 self-nav">
            <ul>
                <li><a style="font-size: 20px">软件工程页面后台管理</a></li>
                <li class="self-right">
                    <a>欢迎您！</a>
                    <a href="<%=basePath%>index">退出登录</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="self-news" style="background: white">
        <form id="newsform" action="add_to_works" method="post" class="layui-form white-bg radius" style="padding:10px 30px; background-color: white">
            <h3 style="text-align: center; margin-bottom: 10px">添加就业信息</h3>
            <div class="layui-form-item">
                <label class="layui-form-label">标题</label>
                <div class="layui-input-block">
                    <input name="title" id="title" class="layui-input" required placeholder="请输入新闻标题">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">分区</label>
                <div class="layui-input-block">
                    <input class="layui-input" value="${workType}" placeholder="${workType}" disabled>
                    <input name="type" value="${workType}" style="display: none">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">内容</label>
                <div class="layui-input-block">
                    <textarea name="content" id="content" style="display: none;"></textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button type="submit" lay-submit="" class="layui-btn layui-btn-radius layui-btn-normal" style="width: 150px" lay-filter="formSubmit" >提交</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['layedit', 'form'], function(){
        var form = layui.form;
        var layedit = layui.layedit;
        var index = layedit.build('content', {
            height: 350,
            tool: [
                'strong' //加粗
                ,'italic' //斜体
                ,'underline' //下划线
                ,'del' //删除线
                ,'|' //分割线
                ,'left' //左对齐
                ,'center' //居中对齐
                ,'right' //右对齐
                ,'link' //超链接
                ,'unlink' //清除链接
            ]
        }); //建立编辑器
        form.verify({
            content: function (value) {
                return layedit.sync(index);
            }
        });
        // form.on("submit(formSubmit)", function (data) {
        //     console.log(data.field);
        //     $.post("layEdit/save", data.field, function (result) {
        //         layer.msg(result.msg, {offset: 'rb'});
        //     })
        // })

    });
</script>
</body>
</html>
