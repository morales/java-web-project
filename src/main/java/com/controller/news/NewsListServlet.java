package com.controller.news;

import com.entity.News;
import com.entity.Notification;
import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@WebServlet("/news")
public class NewsListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<News> newsList = new ArrayList<>();
        String sql = "select * from news";
        String getType = req.getParameter("type");
        String type = null;
        if (getType.equals("college")) {
            type = "学院新闻";
        } else if (getType.equals("party")) {
            type = "党建工作";
        } else if (getType.equals("group")) {
            type = "学团动态";
        }
        try (Connection conn = DataSourceUtils.getConnection();
             PreparedStatement st = conn.prepareStatement(sql)) {
            try(ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String newsType = rs.getString("type");
                    String title = rs.getString("title");
                    String content = rs.getString("content");
                    Date addTime = rs.getDate("addtime");
                    News news = new News(id, title, newsType, content, addTime);
                    if (type.equals(newsType)) {
                        newsList.add(news);
                    }
                }
                Collections.reverse(newsList);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        req.setAttribute("newsList", newsList);
        req.setAttribute("type", type);
        req.getRequestDispatcher("/WEB-INF/news/newslist.jsp").forward(req, resp);
    }
}