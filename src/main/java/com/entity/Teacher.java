package com.entity;

public class Teacher {
    private int id;
    private String name;
    private String grade;
    private String image;
    private String introduction;
    private String email;

    public Teacher() {
    }

    public Teacher(int id, String name, String grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
    }

    public Teacher(int id, String name, String grade, String image, String introduction) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.image = image;
        this.introduction = introduction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

}
