package com.controller.work;

import com.entity.News;
import com.entity.Work;
import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

@WebServlet("/work")
public class GetWorkServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sql = "select * from work where id=?";
        Work work = null;
        try (Connection conn = DataSourceUtils.getConnection();
             PreparedStatement st = conn.prepareStatement(sql)) {
            st.setString(1, req.getParameter("wid"));
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String title = rs.getString("title");
                    Date addTime = rs.getDate("addtime");
                    String type = rs.getString("type");
                    String content = rs.getString("content");
                    work = new Work(id, title, content, type, addTime);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("news", work);
        req.getRequestDispatcher("/WEB-INF/news/work.jsp").forward(req, resp);
    }
}
