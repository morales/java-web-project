package com.controller.backend.teac;

import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/update_teacher")
public class UpdateTeacherServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("tid"));
        String newName = req.getParameter("name");
        String grade = req.getParameter("grade");
        String newGrade = null;
        if (grade.equals("教授")) {
            newGrade = "professor";
        } else if (grade.equals("副教授")) {
            newGrade = "associate";
        } else if (grade.equals("讲师")) {
            newGrade = "lecturer";
        }
        String newContent = req.getParameter("intro");
//        String sql = "update notifications set title=?,content=?,add_time=? where id=?";
        String sql = "update teachers set name=?,grade=?,intro=? where id=?";
        try(Connection conn = DataSourceUtils.getConnection();
            PreparedStatement st = conn.prepareStatement(sql)) {
            st.setString(1, newName);
            st.setString(2, newGrade);
            st.setString(3, newContent);
            st.setInt(4, id);
            st.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        resp.sendRedirect(req.getContextPath()+"/backend/teachers");
    }
}
