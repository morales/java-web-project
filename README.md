# Web Course Project by Morales

## Web开发技术课程设计 - 软件工程网站

### 项目概要

**项目简介**

项目名：moraweb

版本：1.0-SNAPSHOT

项目类型：Maven

语言：JavaWeb

数据库：MySQL

**环境**

操作系统：Windows 10 20H2

IDE：IntelliJ IDEA 2021.2.3

JDK：11.0.2

MarkDown：Typora 0.11.17

本地服务器：tomcat 9.0.52

测试环境：Chrome 98 dev

### 项目部署

生成war包

url：[http://123.57.27.150/2019214204](http://123.57.27.150/2019214204/index)

### 代码编写

静态

> HTML5
>
> CSS3
>
> JavaScript

动态

> Servlet
>
> Java Server Page

外部库

> [Google Material Design Icons](https://google.github.io/material-design-icons/)
>
> [jQuery](https://jquery.com/)
>
> [Bootstrap](https://v4.bootcss.com/)
>
> [Layui](http://layui-doc.pearadmin.com/doc/index.html)
