package com.entity;

import java.util.Date;

public class Work {
    private int id;
    private String title;
    private String content;
    private String type;
    private Date addTime;

    public Work(){
    }

    public Work(int id, String title, String content, String type, Date addTime) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.type = type;
        this.addTime = addTime;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getType() {
        return type;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
