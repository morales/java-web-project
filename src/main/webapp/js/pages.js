$(".self-sidebar h2").click(function () {
    let ul = $(this).next("ul");
    ul.slideToggle();
    $(".self-sidebar ul").not(ul).slideUp();
});