<%--
  Created by IntelliJ IDEA.
  User: Havoc_Wei
  Date: 2021/12/6
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
    <base basePath="<%=basePath%>">
    <title>教师详情 - 软件工程</title>
    <link rel="icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="shortcut icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="bookmark" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link type="text/css" rel="stylesheet" href="<%=basePath%>css/style.css">
    <link type="text/css" rel="stylesheet" href="<%=basePath%>css/pages.css">
    <script src="<%=basePath%>js/jquery-3.5.1.min.js" charset="UTF-8"></script>
</head>
<body>
<div class="self-container">
    <div class="sec-header">
        <div class="logo-site">
            <a href="<%=basePath%>index" title="信息与计算机工程学院软件工程专业">
                <img id="h-logo" src="<%=basePath%>img/logoheader.png">
            </a>
            <a class="sign-in" href="login" target="_blank">
                <span>管理</span>
            </a>
        </div>
    </div>
    <div class="sec-content">
        <div class="desc-content">
            <%@include file="teacherbase.jsp"%>
            <div class="text-content">
                <h1 style="letter-spacing: 5px; color: #181818">${teacher.name}</h1>
                <br>
                <div class="teacher-intro" style="display: flex; flex-direction: row">
                    <img src="<%=basePath%>${teacher.image}" alt="教师照片">
                    <p style="padding-left: 30px; padding-right: 10px; letter-spacing: 1px; line-height: 1.5; text-indent: 2em">${teacher.introduction}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<%=basePath%>js/index.js" charset="UTF-8"></script>
<script src="<%=basePath%>js/pages.js" charset="UTF-8"></script>
<script>
    $("#index").click(function () {
        window.location.href="<%=basePath%>index";
    });
    $("#introduction").click(function () {
        window.location.href="<%=basePath%>/pages/desc.html";
    });
    $("#notification").click(function () {
        window.location.href="javascript:void(0)";
    })
</script>
</body>
</html>
