package com.entity;

import java.util.Date;

public class News {
    private int id;
    private String title;
    private String type;
    private String content;
    private Date addTime;

    public News() {
    }

    public News(int id, String title, Date addTime) {
        this.id = id;
        this.title = title;
        this.addTime = addTime;
    }

    public News(int id, String title, String type, String content, Date addTime) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.content = content;
        this.addTime = addTime;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAddTime(Date addTime) { this.addTime = addTime; }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
