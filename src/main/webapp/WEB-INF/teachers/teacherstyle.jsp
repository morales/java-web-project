<style>
  .teacher-content {
    display: flex;
    flex-flow: row wrap;
    align-items: center;
  }
  .each-teacher {
    width: 30% !important;
    /*display: flex;*/
    /*justify-content: center;*/
    /*align-items: center;*/
    /*flex-direction: column;*/
      margin: 5px auto;
  }

    .teacher-content a {
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        text-decoration: none;
        color: black;
        justify-content: center;
        align-items: center;
    }

  .teacher-content a:hover {
      text-decoration: none;
      color: #302551;
  }

  .each-teacher a img {
    width: 50%;
  }

  .each-teacher a img:hover {
      cursor: url("<%=basePath%>cursor/link.cur"), pointer;
  }
  .each-teacher a p {
    text-align: center;
      font-size: 18px;
  }

  .each-teacher a p:hover {
      cursor: url("<%=basePath%>cursor/link.cur"), pointer;
  }

</style>