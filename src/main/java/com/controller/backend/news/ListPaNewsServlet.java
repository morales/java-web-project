package com.controller.backend.news;

import com.entity.News;
import com.entity.User;
import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@WebServlet("/backend/news/party")
public class ListPaNewsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<News> newsList = new ArrayList<>();
        String sql = "select * from news";
        try (Connection conn = DataSourceUtils.getConnection();
             PreparedStatement st = conn.prepareStatement(sql)) {
            try(ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    if (Objects.equals(rs.getString("type"), "党建工作")) {
                        int id = rs.getInt("id");
                        String title = rs.getString("title");
                        String type = rs.getString("type");
                        String content = rs.getString("content");
                        Date addTime = rs.getDate("addtime");
                        News news = new News(id, title, type, content, addTime);
                        newsList.add(news);
                    }
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        User user = (User) req.getSession().getAttribute("user");
        if (user != null) {
            req.setAttribute("username", user.getUserName());
            req.setAttribute("newsList", newsList);
            req.getRequestDispatcher("/WEB-INF/backend/news2.jsp").forward(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath() + "/login");
        }
    }
}
