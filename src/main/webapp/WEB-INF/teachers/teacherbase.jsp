﻿<div class="d-nav" style="width: 250px">
    <nav>
        <div class="self-sidebar" id="index">
            <h2>首页</h2>
        </div>
        <div class="self-sidebar" id="introduction">
            <h2>专业介绍</h2>
        </div>
        <div class="self-sidebar">
            <h2>实验室介绍</h2>
            <ul>
                <li><a href="<%=basePath%>pages/se.html">软件工程实验室</a></li>
                <li><a href="<%=basePath%>pages/ed.html">嵌入式技术实验室</a></li>
                <li><a href="<%=basePath%>pages/android.html">移动开发实验室</a></li>
                <li><a href="<%=basePath%>pages/ios.html">IOS开发实验室</a></li>
            </ul>
        </div>
        <div class="self-sidebar">
            <h2 style="background-color: #663366">教师队伍</h2>
            <ul>
                <li><a href="<c:url value="/professors"/>">教授</a></li>
                <li><a href="<c:url value="/associates"/>">副教授</a></li>
                <li><a href="<c:url value="/lecturers"/>">讲师</a></li>
            </ul>
        </div>
        <div class="self-sidebar">
            <h2>就业指南</h2>
            <ul>
                <li><a href="<c:url value="/worklist?type=lw"/>">就业工作</a></li>
                <li><a href="<c:url value="/worklist?type=li"/>">就业信息</a></li>
            </ul>
        </div>
        <div class="self-sidebar">
            <h2>新闻资讯</h2>
            <ul>
                <li><a href="<c:url value="/news?type=college"/>">学院新闻</a></li>
                <li><a href="<c:url value="/news?type=party"/>">党建工作</a></li>
                <li><a href="<c:url value="/news?type=group"/>">学团动态</a></li>
            </ul>
        </div>
        <div class="self-sidebar" id="notification">
            <h2>通知公告</h2>
        </div>
    </nav>
</div>