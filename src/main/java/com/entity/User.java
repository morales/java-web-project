package com.entity;

import java.time.LocalDateTime;

public class User {
    private String userName;
    private String userPassword;
    private LocalDateTime localDateTime;

    public User() {
    }

    public User(String name, String password, LocalDateTime localTime) {
        userName = name;
        userPassword = password;
        localDateTime = localTime;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
