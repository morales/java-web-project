<%--
  Created by IntelliJ IDEA.
  User: Havoc_Wei
  Date: 2021/11/21
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>欢迎登录软件工程网站</title>
    <!-- loading logo -->
    <link rel="icon" href="img/logo32.png" type="image/x-icon" />
    <link rel="shortcut icon" href="img/logo32.png" type="image/x-icon" />
    <link rel="bookmark" href="img/logo32.png" type="image/x-icon" />
    <!-- loading layui -->
    <link rel="stylesheet" href="layui/css/layui.css" type="text/css">
    <style>
        #ast {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0px;
            top: 0px;
            object-fit: cover;
        }
        .login{
            position: absolute;
            left: 50%;
            top:50%;
            transform: translate(-50%,-50%);
            width: 400px;
            background-color: rgba(23,11,30,0.3);
            padding:25px 50px;
            border-radius: 10px;
            border:2px solid rgba(142,114,136,0.7);
            backdrop-filter: blur(5px);
            animation-name: fade-down;
            animation-duration: 1s;
        }
        .login .title{
            text-align: center;
            color: #F2F2F2;
            font-weight: bold;
            font-size: 26px;
            padding-bottom: 25px;
            line-height: 60px;
        }
        @keyframes fade-down {
            from{
                transform: translateX(-50%) translateY(-700px);
                opacity: 0;
            }
            to{
                transform: translateX(-50%) translateY(-50%);
                opacity: 1;
            }
        }
        .login .layui-input-block{
            margin-left: 0px;
        }
        .login .layui-input-block .layui-input{
            background-color: rgba(0,0,0,0);
            color: #fff;
        }
        .border-p {
            color: rgb(142,114,136);
            border: 1px solid white;
        }
        .border-p:hover {
            color: whitesmoke;
            border: 1px solid #98677A;
        }
    </style>
</head>
<body onselectstart="return false;" unselectable="on">
<img id="ast" src="img/astronomy.jpg">
<div class="login">
    <div class="title">
        信息与计算机工程学院 软件工程
    </div>
    <form class="layui-form" action="login" method="post">
        <div class="layui-form-item">
            <div class="layui-input-block">
                <input type="text" name="username" required
                       lay-reqText="请输入账号"
                       lay-verify="required" placeholder="请输入学号 / 工号 / 管理员账号" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <input type="password"
                       lay-reqText="请输入密码"
                       name="password" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block" style="text-align: center">
                <button class="layui-btn layui-btn-primary border-p" type="submit">登录</button>
                <button type="reset" class="layui-btn layui-btn-primary border-p">重置</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="layui/layui.js"></script>
<c:if test="${success}">
    <script>
        layer.msg("登陆成功")
    </script>
</c:if>
<c:if test="${success == false}">
    <script>
        layer.msg("用户名或密码错误")
    </script>
</c:if>
<script>
    // layui.use('form', function(){
    //     var form = layui.form;
    //     console.log(form)
    //     //监听提交
    //     form.on('submit(formDemo)', function(data){
    //         // layer.msg(JSON.stringify(data.field));
    //         return true;
    //     });
    //
    // });
</script>
</body>
</html>

