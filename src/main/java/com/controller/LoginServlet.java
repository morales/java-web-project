package com.controller;

import com.entity.User;
import com.util.DataBaseUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("username");
        String userPassword = req.getParameter("password");
        User user = DataBaseUtils.getUser(userName, userPassword);
        String url = null;
        if (user != null) {
            req.getSession().setAttribute("user", user);
            url = "/backend/notification";
        } else {
            req.setAttribute("success", false);
            url = "/login";
        }
        resp.sendRedirect(req.getContextPath() + url);
    }
}
