package com.controller.work;

import com.entity.Work;
import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@WebServlet("/worklist")
public class WorkListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Work> workList = new ArrayList<>();
        String sql = "select * from work";
        String getType = req.getParameter("type");
        String type = null;
        if (getType.equals("lw")) {
            type = "就业工作";
        } else if (getType.equals("li")) {
            type = "就业信息";
        }
        try (Connection conn = DataSourceUtils.getConnection();
             PreparedStatement st = conn.prepareStatement(sql)) {
            try(ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String newsType = rs.getString("type");
                    String title = rs.getString("title");
                    String content = rs.getString("content");
                    Date addTime = rs.getDate("addtime");
                    Work news = new Work(id, title, newsType, content, addTime);
                    if (type.equals(newsType)) {
                        workList.add(news);
                    }
                }
                Collections.reverse(workList);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        req.setAttribute("workList", workList);
        req.setAttribute("type", type);
        req.getRequestDispatcher("/WEB-INF/news/workin.jsp").forward(req, resp);
    }
}
