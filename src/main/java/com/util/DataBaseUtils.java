package com.util;

import com.entity.User;

import java.time.LocalDateTime;

public class DataBaseUtils {
    public static User getUser(String name, String pwd) {
        User user = null;
        if (("admin".equals(name) && "admin".equals(pwd))) {
            user = new User(name, pwd, LocalDateTime.now());
        } else if ("2019214204".equals(name) && "2019214204".equals(pwd)) {
            user = new User("作者", pwd, LocalDateTime.now());
        }
        return user;
    }
}
