<%--
  Created by IntelliJ IDEA.
  User: Havoc_Wei
  Date: 2021/12/6
  Time: 20:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
    <base basePath="<%=basePath%>">
    <title>副教授 - 软件工程</title>
    <meta charset="UTF-8">
    <link rel="icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="shortcut icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="bookmark" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link type="text/css" rel="stylesheet" href="<%=basePath%>css/style.css">
    <link type="text/css" rel="stylesheet" href="<%=basePath%>css/pages.css">
    <%@include file="teacherstyle.jsp"%>
    <script src="<%=basePath%>js/jquery-3.5.1.min.js" charset="UTF-8"></script>
</head>
<body>
<div class="self-container">
    <div class="sec-header">
        <div class="logo-site">
            <a href="<%=basePath%>index" title="信息与计算机工程学院软件工程专业">
                <img id="h-logo" src="<%=basePath%>img/logoheader.png">
            </a>
            <a class="sign-in" href="login">
                <span>管理</span>
            </a>
        </div>
    </div>
    <div class="sec-content">
        <div class="desc-content">
            <%@include file="teacherbase.jsp"%>
            <div class="text-content">
                <h1 style="letter-spacing: 5px; color: #181818">副教授列表</h1> <br>
                <div class="teacher-content">
                    <c:forEach var="t" items="${associates}">
                        <div class="each-teacher">
                            <a href="teachers?tid=${t.id}" title="${t.name}">
                                <img src="<%=basePath%>${t.image}" alt="${t.name}">
                                <p>${t.name}</p>
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="teacherjs.jsp"%>
</body>
</html>
