<%--
  Created by IntelliJ IDEA.
  User: Havoc_Wei
  Date: 2021/12/12
  Time: 0:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
    <meta charset="UTF-8">
    <title>${type} - 软件工程</title>
    <link rel="icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="shortcut icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="bookmark" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link type="text/css" rel="stylesheet" href="<%=basePath%>css/style.css">
    <link type="text/css" rel="stylesheet" href="<%=basePath%>css/pages.css">
    <style>
        .each-notice {
            border-bottom: #d0d0d0 solid 1px;
            margin-right: 15px;
            margin-left: 5px;
            margin-bottom: 0;
            height: 45px;
            line-height: 45px;
            overflow: hidden;
            display: flex;
            flex-direction: row;
            /*padding: 5px 10px;*/
        }
        .each-notice a {
            color: black;
            padding-left: 20px;
            font-size: 16px;
        }
        <%--.each-notice:hover {--%>
        <%--    cursor: url("<%=basePath%>cursor/link.cur"), pointer;--%>
        <%--}--%>
        .each-notice a:hover {
            color: #663344;
            cursor: url("<%=basePath%>cursor/link.cur"), pointer;
        }
        .each-notice span {
            margin-left: auto;
        }
    </style>
    <script src="<%=basePath%>js/jquery-3.5.1.min.js" charset="UTF-8"></script>
</head>
<body>
<div class="self-container">
    <div class="sec-header">
        <div class="logo-site">
            <a href="<%=basePath%>index" title="信息与计算机工程学院软件工程专业">
                <img id="h-logo" src="<%=basePath%>img/logoheader.png">
            </a>
            <a class="sign-in" href="login">
                <span>管理</span>
            </a>
        </div>
    </div>
    <div class="sec-content">
        <div class="desc-content">
            <div class="d-nav" style="width: 250px">
                <nav>
                    <div class="self-sidebar" id="index">
                        <h2>首页</h2>
                    </div>
                    <div class="self-sidebar" id="introduction">
                        <h2>专业介绍</h2>
                    </div>
                    <div class="self-sidebar">
                        <h2>实验室介绍</h2>
                        <ul>
                            <li><a href="<%=basePath%>pages/se.html">软件工程实验室</a></li>
                            <li><a href="<%=basePath%>pages/ed.html">嵌入式技术实验室</a></li>
                            <li><a href="<%=basePath%>pages/android.html">移动开发实验室</a></li>
                            <li><a href="<%=basePath%>pages/ios.html">IOS开发实验室</a></li>
                        </ul>
                    </div>
                    <div class="self-sidebar">
                        <h2>教师队伍</h2>
                        <ul>
                            <li><a href="<c:url value="/professors"/>">教授</a></li>
                            <li><a href="<c:url value="/associates"/>">副教授</a></li>
                            <li><a href="<c:url value="/lecturers"/>">讲师</a></li>
                        </ul>
                    </div>
                    <div class="self-sidebar">
                        <h2>就业指南</h2>
                        <ul>
                            <li><a href="<c:url value="/worklist?type=lw"/>">就业工作</a></li>
                            <li><a href="<c:url value="/worklist?type=li"/>">就业信息</a></li>
                        </ul>
                    </div>
                    <div class="self-sidebar">
                        <h2 style="background-color: #663366">新闻资讯</h2>
                        <ul>
                            <li><a href="<c:url value="/news?type=college"/>">学院新闻</a></li>
                            <li><a href="<c:url value="/news?type=party"/>">党建工作</a></li>
                            <li><a href="<c:url value="/news?type=group"/>">学团动态</a></li>
                        </ul>
                    </div>
                    <div class="self-sidebar" id="notification">
                        <h2>通知公告</h2>
                    </div>
                </nav>
            </div>
            <div class="text-content">
                <h1 style="letter-spacing: 5px; color: #181818">${type}</h1> <br>
                <ul class="notice-content">
                    <c:forEach var="n" items="${newsList}">
                        <li class="each-notice">
                            <a href="content?nid=${n.id}" target="_blank" title="${n.title}">${n.title}</a>
                            <span>${n.addTime}</span>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="<%=basePath%>js/index.js" charset="UTF-8"></script>
<script src="<%=basePath%>js/pages.js" charset="UTF-8"></script>
<script>
    $("#index").click(function () {
        window.location.href="<%=basePath%>index";
    });
    $("#introduction").click(function () {
        window.location.href="<%=basePath%>pages/desc.html";
    });
    $("#notification").click(function () {
        window.location.href="<c:url value="/notifications"/>";
    })
</script>
</body>
</html>
