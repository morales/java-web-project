package com.controller.backend.noti;

import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

@WebServlet("/add_notification")
public class AddNotificationServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        int id = Integer.parseInt(req.getParameter("nid"));
        System.out.println("title: "+req.getParameter("a-title"));
        String newTitle = req.getParameter("a-title");
        String newContent = req.getParameter("a-content");
        Date date = new Date();
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
//        System.out.println("date: "+ date);
//        String sql = "update notifications set title=?,content=?,add_time=? where id=?";
        String sql = "insert into notifications (title, content, add_time, image) VALUES (?, ?, ?, null)";
//        System.out.println("date: "+ sqlDate);
        try(Connection conn = DataSourceUtils.getConnection();
            PreparedStatement st = conn.prepareStatement(sql)) {
            st.setString(1, newTitle);
            st.setString(2, newContent);
            st.setDate(3, sqlDate);
            st.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        resp.sendRedirect(req.getContextPath()+"/backend/notification");
    }
}
