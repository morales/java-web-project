package com.controller.backend.news;

import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Objects;

@WebServlet("/add_to_news")
public class AddToNewsServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newTitle = req.getParameter("title");
        String newType = req.getParameter("type");
        String newContent = req.getParameter("content");
//        System.out.println("newTitle: " + newTitle);
//        System.out.println("newType: " + newType);
//        System.out.println("newContent: " + newContent);
        Date date = new Date();
        java.sql.Date newDate = new java.sql.Date(date.getTime());
        String sql = "insert into news (type, title, content, addtime) values (?, ?, ?, ?)";
        try(Connection conn = DataSourceUtils.getConnection();
            PreparedStatement st = conn.prepareStatement(sql)) {
            st.setString(1, newType);
            st.setString(2, newTitle);
            st.setString(3, newContent);
            st.setDate(4, newDate);
            st.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (Objects.equals(newType, "学院新闻")) {
            resp.sendRedirect(req.getContextPath()+"/backend/news/college");
        } else if (Objects.equals(newType, "党建工作")) {
            resp.sendRedirect(req.getContextPath()+"/backend/news/party");
        } else if (Objects.equals(newType, "学团动态")) {
            resp.sendRedirect(req.getContextPath()+"/backend/news/group");
        }
    }
}
