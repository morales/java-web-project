<%--
  Created by IntelliJ IDEA.
  User: Havoc_Wei
  Date: 2021/12/15
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
    <c:url var="base" value="/"/>
    <base href="${base}">
    <meta charset="UTF-8">
    <title>教师管理</title>
    <link rel="icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="shortcut icon" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link rel="bookmark" href="<%=basePath%>img/logo32.png" type="image/x-icon" />
    <link href="https://cdn.bootcss.com/material-design-icons/3.0.1/iconfont/material-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://getbootstrap.net/Application/Home/View/Public/css/bootstrap.min.css">
    <script src="<%=basePath%>js/jquery-3.5.1.min.js" charset="UTF-8"></script>
    <script src="https://cdn.staticfile.org/popper.js/2.9.3/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/5.1.1/js/bootstrap.min.js"></script>
    <style>
        * {
            /*padding: 0 !important;*/
            /*margin: 0 !important;*/
            box-sizing: border-box !important;
        }

        a {
            text-decoration: none  !important;
        }

        .self-container {
            height: 100vh ;
            background-color: #F0F0F0 ;
        }

        .self-row {
            display: flex !important;
            align-items: flex-start ;
            background-color: #F0F0F0 ;
        }

        .self-row .grow-self {
            flex-grow: 1 ;
            height: auto ;
            padding: 20px ;
        }

        .self-nav ul {
            display: flex !important;
            background: #333333 !important;
            list-style: none !important;
            margin: 0 !important;
        }

        .self-nav ul a {
            color: white !important;
            text-decoration: none !important;
            display: block !important;
            padding: 15px 25px !important;
        }

        .self-nav ul a:hover {
            background: #111 !important;
        }

        .self-nav .self-right {
            margin-left: auto  !important;
            display: flex ;
            flex-direction: row ;
        }

        .self-sidebar {
            min-width: 200px ;
        }
        .self-sidebar-group {
            background: gray ;
        }
        .self-sidebar-group > ul {
            list-style: none !important;
            margin: 0 !important;
            padding: 0 !important;
        }
        .self-sidebar-group > h2 {
            background: #6eb6ff !important;
            color: white !important;
            padding: 10px 20px !important;
            margin: 0 !important;
        }
        .self-sidebar-group .member{
            display: block !important;
            color: black !important;
            text-decoration: none !important;
            padding: 10px 20px !important;
            transition: transform 0.5s !important;
        }
        .self-sidebar-group .member:hover {
            background: #4476A7 !important;
            color: white !important;
            transform: scale(1.1) !important;
        }
        .footer {
            height: 80px ;
            bottom: 0 ;
            width: 100% ;
            margin: auto ;
            text-align: center ;
            background-color: #333333 ;
            color: white ;
        }

        .footer p{
            padding: 0 0 !important;
            margin: 0 !important;
        }
        .col-md-1 {width: 8.33%; }
        .col-md-2 {width: 16.67%; }
        .col-md-3 {width: 25%; }
        .col-md-4 {width: 33.33%; }
        .col-md-5 {width: 41.67%; }
        .col-md-6 {width: 50%; }
        .col-md-7 {width: 58.33%; }
        .col-md-8 {width: 66.67%; }
        .col-md-9 {width: 75%; }
        .col-md-10 {width: 83.33%; }
        .col-md-11 {width: 91.67%; }
        .col-md-12 {width: 100%; padding: 0; margin: 0}

        .modal-self-table {
            width: 100% !important;
            border-collapse: collapse !important;
        }

        .modal-self-table th, .modal-self-table td {
            text-align: center !important;
            padding: 10px !important;
            border-bottom: 1px solid #ddd !important;
            overflow: hidden !important;
        }

        .modal-self-table tbody tr {
            background-color: white !important;
        }
        .modal-self-table tbody tr:hover {
            background-color: rgba(128, 128, 128, 0.6) ;
        }

        table {
            width: 100% !important;
            border-collapse: collapse !important;
            margin-left: auto !important;
        }

        table th, table td {
            text-align: center !important;
            padding: 10px !important;
            border-bottom: 1px solid #ddd !important;
            overflow: hidden !important;
        }
        tbody tr:nth-child(even) {
            background-color: #f0f0f0 ;
        }
        tbody tr:nth-child(odd) {
            background-color: #cbe5fa;
        }
        tbody tr:hover {
            background-color: rgba(128, 128, 128, 0.6) ;
        }
        .btn-change {
            color: #fff ;
            background-color: #007bff ;
            border-color: #007bff ;
            padding: 10px 25px  !important;
            text-decoration: none ;
            display: inline-block ;
            border-radius: 8px ;
        }

        .btn-change:hover {
            color: #fff ;
            background-color: #0069d9 ;
            border-color: #0062cc ;
        }

        .btn-delete {
            color: #fff !important;
            background-color: #dc3545 ;
            border-color: #dc3545 ;
            padding: 10px 25px  !important;
            text-decoration: none ;
            display: inline-block ;
            border-radius: 8px ;
        }

        .btn-delete:hover {
            color: #fff ;
            background-color: #c82333 ;
            border-color: #bd2130 ;
        }

        .modal-form {
            display: flex !important;
            flex-direction: column !important;
        }
    </style>
</head>
<body>
<div class="self-container">
    <div class="self-area self-row">
        <div class="col-md-12 self-nav">
            <ul>
                <li><a style="font-size: 20px">软件工程页面后台管理</a></li>
                <li class="self-right">
                    <a>欢迎您！${username}</a>
                    <a href="<%=basePath%>index">退出登录</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="self-area self-row">
        <div class="self-area self-sidebar">
            <div class="self-sidebar-group">
                <h2>教师管理</h2>
            </div>
            <div class="self-sidebar-group">
                <h2>就业管理</h2>
                <ul>
                    <li><a class="member" href="<%=basePath%>backend/works?workType=lw">就业工作</a></li>
                    <li><a class="member" href="<%=basePath%>backend/works?workType=li">就业信息</a></li>
                </ul>
            </div>
            <div class="self-sidebar-group">
                <h2>新闻管理</h2>
                <ul>
                    <li><a href="<%=basePath%>backend/news/college" class="member">学院新闻</a></li>
                    <li><a href="<%=basePath%>backend/news/party" class="member">党建工作</a></li>
                    <li><a href="<%=basePath%>backend/news/group" class="member">学团动态</a></li>
                </ul>
            </div>
            <div class="self-sidebar-group">
                <h2><a style="color: white" href="<%=basePath%>backend/notification">通知管理</a></h2>
            </div>
        </div>
        <div class="self-area grow-self">
            <form>
                <table id="table">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>姓名</th>
                        <th>等级</th>
                        <th>照片</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="n" items="${teachers}" varStatus="i">
                        <tr>
                            <td><strong>${n.id}</strong></td>
                            <td>${n.name}</td>
                            <td>${n.grade}</td>
                            <td><img src="<%=basePath%>${n.image}" style="width: 10%"></td>
                            <td style="display: none;">${n.introduction}</td>
                            <td>
                                <a href="javascript:void(0)" class="btn-change" id="${i.count}" data-bs-toggle="modal" data-bs-target="#update-modal" onclick="editInfo(this)"><i class="material-icons">edit</i></a>
                                </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="self-area footer self-row">
        <div class="col-md-12">
            <p style="padding-top: 10px">东北林业大学<br>
                软件工程专业 Morales&copy<br>
                欢迎访问：<a target="_blank" style="text-decoration: none; color: white" href="https://space.bilibili.com/350869102">https://space.bilibili.com/350869102</a> </p>
        </div>
    </div>
</div>

<%--Modal--%>
<div class="modal fade" id="update-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">修改教师信息</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form  class="modal-form" action="update_teacher" method="post">
                <div class="modal-body">
                    <table class="modal-self-table">
                        <tr>
                            <td class="col-md-2">编号：</td>
                            <td class="col-md-4"><input type="text" name="id" id="id" style="width: 100%;" disabled /></td>
                            <td class="col-md-4" style="display: none"><input type="text" name="tid" id="tid"/></td>
                        </tr>
                        <tr>
                            <td>姓名：</td>
                            <td><input type="text" name="name" id="name" style="width: 100%;" required/></td>
                        </tr>
                        <tr>
                            <td>等级：</td>
                            <td><input type="text" name="grade" id="grade" style="width: 100%;" required/></td>
                        </tr>
                        <tr>
                            <td>介绍：</td>
                            <td><textarea name="intro" id="intro" rows="5" cols="48" required></textarea></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">取消</button>
                    <button type="submit" name="submit" class="btn btn-primary">修改</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function editInfo(oneObject) {
        let newId = $(oneObject).attr("id");
        let id = document.getElementById("table").rows[newId].cells[0].innerText;
        let name = document.getElementById("table").rows[newId].cells[1].innerText;
        let grade = document.getElementById("table").rows[newId].cells[2].innerText;
        let intro = document.getElementById("table").rows[newId].cells[4].innerText;
        $('#id').val(id);
        $('#tid').val(id.toString());
        $('#name').val(name);
        $('#grade').val(grade);
        $('#intro').val(intro);
    }
</script>
</body>
</html>
