package com.controller.backend.noti;

import com.entity.Notification;
import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

@WebServlet("/update_notification")
public class UpdateNotificationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("title: "+req.getParameter("nid"));
        int id = Integer.parseInt(req.getParameter("nid"));
        String newTitle = req.getParameter("title");
        String newContent = req.getParameter("content");
//        String sql = "update notifications set title=?,content=?,add_time=? where id=?";
        String sql = "update notifications set title=?,content=? where id=?";
        try(Connection conn = DataSourceUtils.getConnection();
            PreparedStatement st = conn.prepareStatement(sql)) {
            st.setString(1, newTitle);
            st.setString(2, newContent);
            st.setInt(3, id);
            st.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        resp.sendRedirect(req.getContextPath()+"/backend/notification");
    }
}
