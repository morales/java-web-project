package com.controller.backend.news;

import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet("/delete_news")
public class DeleteNewsServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("getId = " + req.getParameter("noid"));
        int id = Integer.parseInt(req.getParameter("noid"));
        String type = req.getParameter("netype");
        String sql = "delete from news where id=?";
        try(Connection conn = DataSourceUtils.getConnection();
            PreparedStatement st = conn.prepareStatement(sql)) {
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        if (Objects.equals(type, "学院新闻")) {
            resp.sendRedirect(req.getContextPath()+"/backend/news/college");
        } else if (Objects.equals(type, "党建工作")) {
            resp.sendRedirect(req.getContextPath()+"/backend/news/party");
        } else if (Objects.equals(type, "学团动态")) {
            resp.sendRedirect(req.getContextPath()+"/backend/news/group");
        }

    }
}
