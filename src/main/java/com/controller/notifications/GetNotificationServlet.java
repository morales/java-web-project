package com.controller.notifications;

import com.entity.Notification;
import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

@WebServlet("/notification")
public class GetNotificationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Notification notification = null;
        String sql = "select * from notifications where id=?";
        try (Connection conn = DataSourceUtils.getConnection();
             PreparedStatement st = conn.prepareStatement(sql)) {
            st.setString(1, req.getParameter("nid"));
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String title = rs.getString("title");
                    Date addTime = rs.getDate("add_time");
                    String image = rs.getString("image");
                    String content = rs.getString("content");
                    String contentReplace = content.replace("-n", "</p><p>");
                    contentReplace = "<p>" + contentReplace + "</p>";
                    notification = new Notification(id, title, image, contentReplace, addTime);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        req.setAttribute("notification", notification);
        req.getRequestDispatcher("/WEB-INF/notifications/notice.jsp").forward(req, resp);
    }
}
