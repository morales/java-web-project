package com.controller.backend.news;

import com.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/add_news")
public class AddNewsServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("newstype");
        req.setAttribute("newstype", type);
        req.getRequestDispatcher("/WEB-INF/backend/newsadd.jsp").forward(req, resp);
    }
}
