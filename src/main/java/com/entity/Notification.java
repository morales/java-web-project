package com.entity;

import java.util.Date;

public class Notification {
    private int id;
    private String title;
    private String image;
    private String content;
    private Date addTime;

    public Notification() {
    }

    public Notification(int id, String title, Date addTime) {
        this.id = id;
        this.title = title;
        this.addTime = addTime;
    }

    public Notification(int id, String title, String content, Date addTime) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.addTime = addTime;
    }

    public Notification(int id, String title, String image, String content, Date addTime) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.content = content;
        this.addTime = addTime;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getContent() {
        return content;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
