<%--
  Created by IntelliJ IDEA.
  User: Havoc_Wei
  Date: 2021/12/17
  Time: 13:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,initial-scale=1.0" />
    <title>欢迎访问东北林业大学软件工程网站</title>
    <meta name="keywords" content="软件工程,东北林业大学,信息与计算机工程学院">
    <meta name="description" content="软件工程专业以IT业需求为导向，培养具有良好综合素质和职业道德，掌握扎实的基础理论和专业知识，具有软件分析、设计、开发、测试与管理能力，具备较强工程实践能力、技术创新能力和团队精神，能快速适应软件工程新技术发展并具有国际视野和国际竞争力的高级软件工程师。">
<%--     logo --%>
    <link rel="icon" href="img/logo32.png" type="image/x-icon" />
    <link rel="shortcut icon" href="img/logo32.png" type="image/x-icon" />
    <link rel="bookmark" href="img/logo32.png" type="image/x-icon" />
    <link type="text/css" rel="stylesheet" href="css/style.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
<%--     loading google material icon --%>
    <link href="https://cdn.bootcss.com/material-design-icons/3.0.1/iconfont/material-icons.css" rel="stylesheet">
<%--     loading layui --%>
    <link type="text/css" rel="stylesheet" href="layui/css/layui.css">
    <script type="text/javascript" src="layui/layui.js"></script>
</head>
<body onselectstart="return false;" unselectable="on">

<div class="self-container">
    <div id="hea" class="header">
        <div class="logo-site">
            <a id="a-logo" href="index" title="信息与计算机工程学院软件工程专业">
                <img id="h-logo" src="img/logoheader.png">
            </a>
            <a class="sign-in" href="login">
                <span>管理</span>
            </a>
        </div>
        <div class="header-nav">
            <div class="header-nav-item"><a href="index">首页</a></div>
            <div class="header-nav-item"><a href="pages/desc.html">专业介绍</a></div>
            <div class="header-nav-item">
                <a>实验室介绍</a>
                <div class="header-nav-second">
                    <ul>
                        <li><a href="pages/se.html">软件工程实验室</a></li>
                        <li><a href="pages/ed.html">嵌入式技术实验室</a></li>
                        <li><a href="pages/android.html">移动开发实验室</a></li>
                        <li><a href="pages/ios.html">IOS开发实验室</a></li>
                    </ul>
                </div>
            </div>
            <div class="header-nav-item">
                <a>教师队伍</a>
                <div class="header-nav-second">
                    <ul>
                        <li><a href="professors">教授</a></li>
                        <li><a href="associates">副教授</a></li>
                        <li><a href="lecturers">讲师</a></li>
                    </ul>
                </div>
            </div>
            <div class="header-nav-item">
                <a>就业指南</a>
                <div class="header-nav-second">
                    <ul>
                        <li><a href="worklist?type=lw">就业工作</a></li>
                        <li><a href="worklist?type=li">就业信息</a></li>
                    </ul>
                </div>
            </div>
            <div class="header-nav-item">
                <a href="#">新闻资讯</a>
                <div class="header-nav-second">
                    <ul>
                        <li><a href="news?type=college">学院新闻</a></li>
                        <li><a href="news?type=party">党建工作</a></li>
                        <li><a href="news?type=group">学团动态</a></li>
                    </ul>
                </div>
            </div>
            <div class="header-nav-item"><a href="notifications">通知公告</a></div>
        </div>
    </div>

    <div class="content">
        <div class="self-banner">
            <ul class="self-banner-img">
                <li><img src="img/banner00.png"></li>
                <li><img src="img/banner01.png"></li>
                <li><img src="img/banner02.png"></li>
                <li><img src="img/banner03.png"></li>
            </ul>
            <div class="self-banner-list"></div>
            <div class="banner-arrow-left"><img src="img/left_arrow.png"></div>
            <div class="banner-arrow-right"><img src="img/right_arrow.png"></div>
        </div>
        <div class="index-contents">
            <div class="index-content" style="margin-left: 20px; margin-right: 10px">
                <div class="topper" style="color: #441435;">
                    <div style="display: flex; flex-direction: row; justify-content: center">
                        <h1><i class="material-icons" style="font-size: 40px">chrome_reader_mode</i></h1>
                        <h1 style="margin-left: 5px">新闻中心</h1>
                    </div>
                </div>
                <ul class="news-notis">
                    <c:forEach var="n" items="${news}" begin="0" end="9">
                        <li>
                            <a class="news-notis-content" style="display: flex; flex-direction: row" href="content?nid=${n.id}" target="_blank" title="${n.title}">
                                <span style="width: 85%; font-size: 18px; display: inline; word-break:keep-all; overflow: hidden; text-overflow: ellipsis">${n.title}</span>
                                <span style="width: 15%; line-height: 24px; margin-left: auto; text-align: center; justify-content: center">${n.addTime}</span>
                            </a>
                            <hr class="layui-border-black">
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="index-content" style="margin-left: 10px; margin-right: 20px">
                <div class="topper" style="color: #441435;">
                <div style="display: flex; flex-direction: row; justify-content: center">
                    <h1><i class="material-icons" style="font-size: 40px">assignment</i></h1>
                    <h1 style="margin-left: 5px">通知公告</h1>
                </div>
            </div>
                <c:forEach var="n" items="${notifications}" begin="0" end="9">
                    <li>
                        <a class="news-notis-content" style="display: flex; flex-direction: row" href="notification?nid=${n.id}" target="_blank" title="${n.title}">
                            <span style="width: 85%; font-size: 18px; display: inline; word-break:keep-all; overflow: hidden; text-overflow: ellipsis">${n.title}</span>
                            <span style="width: 15%; line-height: 24px; margin-left: auto; text-align: center; justify-content: center">${n.addTime}</span>
                        </a>
                        <hr class="layui-border-cyan">
                    </li>
                </c:forEach>
            </div>
        </div>
    </div>

    <div class="layui-footer layui-bg-black">
        <div style="padding: 10px 100px; width: auto; display: flex; flex-direction: row">
            <div style="color: white">
                <p style="margin-top: 0.5em; margin-bottom: 0.5em">东北林业大学信息与计算机工程学院</p>
                <p style="margin-top: 0.5em; margin-bottom: 0.5em">2021 软件工程专业 &copy</p>
                <p style="margin-top: 0.5em; margin-bottom: 0.5em">地址：东北林业大学成栋楼九楼</p>
                <p style="margin-top: 0.5em; margin-bottom: 0.5em">Tel：00000000</p>
                <p style="margin-top: 0.5em; margin-bottom: 0.5em">Fax：11111111</p>
            </div>
            <div style="margin-left: auto">
                <img style="width: 110px" src="img/icec.png" alt="信息与计算机工程学院官网">
                <p style="margin-top: 0.5em">信息与计算机工程学院</p>
            </div>
        </div>
    </div>
</div>
<a href="javascript:void(0);" id="to-top" title="回到顶部" class="material-icons" style="font-size: 45px">keyboard_arrow_up</a>
<script src="js/jquery-3.5.1.min.js" charset="UTF-8"></script>
<script src="js/index.js" type="text/javascript" charset="UTF-8"></script>
<script>
    $(function() {
        $(".self-container").scroll(function() {
            let topp = $(this).scrollTop();
            if (topp > 200) {
                $("#to-top").stop().fadeIn(100);
            } else {
                $("#to-top").stop().fadeOut(100);
            }
        });
        $("#to-top").click(function () {
            $(".self-container").animate({scrollTop:0}, 300);
        });
    });
</script>
</body>

</html>
