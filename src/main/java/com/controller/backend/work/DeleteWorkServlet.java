package com.controller.backend.work;

import com.util.DataSourceUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet("/delete_work")
public class DeleteWorkServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("noid"));
        String toDeleteType = req.getParameter("netype");
        System.out.println("1 " + toDeleteType);
        String sql = "delete from work where id=?";
        try(Connection conn = DataSourceUtils.getConnection();
            PreparedStatement st = conn.prepareStatement(sql)) {
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (Objects.equals(toDeleteType, "就业工作")) {
            resp.sendRedirect(req.getContextPath()+"/backend/works?workType=lw");
        } else if (Objects.equals(toDeleteType, "就业信息")) {
            resp.sendRedirect(req.getContextPath()+"/backend/works?workType=li");
        }

    }
}
